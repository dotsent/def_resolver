require 'def_resolver/version'

require 'active_support/core_ext/object'
require 'active_support/core_ext/module'
require 'active_support/concern'

require 'def_resolver/errors'
require 'def_resolver/utils'
require 'def_resolver/info'
require 'def_resolver/http_transport'
require 'def_resolver/megafon_mnp'

require 'connection_pool'
require 'faraday'
require 'faraday_middleware'

module DefResolver
  DEFAULT_USER_AGENT = "DefResolver Ruby Client #{DefResolver::VERSION}".freeze

  mattr_accessor :default_resolver_class do
    MegafonMnp
  end
  mattr_accessor :proxy
  mattr_accessor :user_agent do
    DEFAULT_USER_AGENT
  end
  mattr_accessor :logger

  module_function

  # Setup
  def setup
    yield self
  end

  # @param [#to_s] msisdn
  # @return [Boolean]
  def can_resolve?(msisdn)
    default_resolver_class.can_resolve?(msisdn)
  end

  # @param [#to_s] msisdn
  # @return [DefResolver::Info]
  def resolve(msisdn)
    default_resolver.resolve(msisdn)
  end

  def default_resolver
    @default_resolver ||= default_resolver_class.new
  end

  def fetch_user_agent
    Utils.random_value(user_agent)
  end

  def fetch_proxy_options
    return unless proxy.present?

    selected_proxy = Utils.random_value(proxy)
    Faraday::ProxyOptions.from(selected_proxy)
  end
end
