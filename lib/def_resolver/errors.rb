module DefResolver
  module Errors
    BaseError = Class.new(RuntimeError)
    UnableToResolve = Class.new(BaseError)
  end
end
