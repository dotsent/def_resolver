module DefResolver
  module HttpTransport
    extend ActiveSupport::Concern

    DEFAULT_TIMEOUT = 30
    POOL_SIZE = 30
    POOL_TIMEOUT = 10

    # @return [Faraday::Connection]
    def pool
      @pool ||= ConnectionPool::Wrapper.new(size: POOL_SIZE, timeout: POOL_TIMEOUT) do
        Faraday.new(url: @options[:base_url], proxy: DefResolver.proxy, &method(:prepare_connection))
      end
    end

    alias conn pool
  end
end
