module DefResolver
  # :reek:TooManyConstants
  class Info
    MCC_RUSSIA = '250'.freeze

    # https://ru.wikipedia.org/wiki/MNC
    # rubocop:disable Style/AsciiComments
    MNC_MTS = '01'.freeze
    MNC_MEGAFON = '02'.freeze
    MNC_ROLTELEKOM_1 = '03'.freeze # (НСС, Элайн)
    MNC_ETK = '05'.freeze
    MNC_SMARTS = '07'.freeze
    MNC_YOTA = '11'.freeze
    MNC_OSNOVA_TELEKOM = '18'.freeze
    MNC_TELE2 = '20'.freeze
    MNC_WIN_MOBILE = '32'.freeze # ("К-Телеком", Республика Крым)
    MNC_MOTIV = '35'.freeze
    MNC_SKYLINK = '37'.freeze
    MNC_ROSTELECOM_2 = '39'.freeze # (Utel, БайкалВестКом, Волгоград-GSM, АКОС, НСС, Тамбов GSM)
    MNC_BEELINE = '99'.freeze # (НТК)
    # rubocop:enable Style/AsciiComments

    attr_reader :msisdn
    attr_reader :mcc
    attr_reader :mnc

    attr_reader :region_code
    attr_reader :region_value

    # @param [#to_s] mnc
    # @return [String]
    # :reek:TooManyStatements
    def self.operator_name(mnc)
      mnc = mnc.to_s
      constants = self.constants.select { |name| name.to_s[0..2] == 'MNC' }
      index = constants.find_index { |name| const_get(name) == mnc }
      return unless index

      constants[index].to_s[4..-1]
    end

    # @param [Hash] attrs
    # @option attrs [String] :msisdn
    # @option attrs [String] :mcc
    # @option attrs [String] :mnc
    # @option attrs [String] :region_code
    # @option attrs [String] :region_value
    # :reek:ManualDispatch
    def initialize(attrs)
      attrs.each do |name, value|
        instance_variable_set(:"@#{name}", value) if respond_to?(name)
      end
    end

    def to_hash
      instance_values.as_json
    end
  end
end
