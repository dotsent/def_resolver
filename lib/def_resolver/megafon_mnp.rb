module DefResolver
  class MegafonMnp
    include ::DefResolver::HttpTransport

    DEFAULT_API_BASE_URL = 'https://www.megafon.ru/api'.freeze
    MSISDN_PREFIX = '7'.freeze
    MSISDN_LENGTH = 11

    class << self
      # @param [#to_s] msisdn
      # @return [Boolean]
      def can_resolve?(msisdn)
        prepared = prepare(msisdn)
        prepared.start_with?(MSISDN_PREFIX) && prepared.length == MSISDN_LENGTH
      end

      private

      def prepare(msisdn)
        msisdn.to_s.tr('^A-Za-z0-9', '')
      end
    end

    def initialize(options = {})
      @options = options

      @options[:base_url] = DEFAULT_API_BASE_URL unless @options[:base_url]
    end

    delegate :can_resolve?, to: self

    # @param [#to_s] msisdn
    # @return [DefResolver::Info]
    # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    # :reek:DuplicateMethodCall { allow_calls: ['json_response'] }
    # :reek:TooManyStatements
    def resolve(msisdn)
      unless can_resolve?(msisdn)
        raise ::DefResolver::Errors::UnableToResolve, "Unable to resolve msisdn=#{msisdn} via #{self.class}"
      end

      response = make_api_request('mfn/info', msisdn: msisdn)
      json_response = response.body

      raise DefResolver::Errors::UnableToResolve, "Unsuccessful api response: #{json_response}" unless response.success?
      raise DefResolver::Errors::UnableToResolve, json_response['error'] if json_response['error']

      ::DefResolver::Info.new(
        msisdn: msisdn.to_i,
        mcc: ::DefResolver::Info::MCC_RUSSIA,
        mnc: region_leading_zero(json_response['operator_id']),
        region_code: json_response['region_id'],
        region_value: json_response['region']
      )
    rescue Faraday::ClientError => error
      raise DefResolver::Errors::UnableToResolve, error.message
    end
    # rubocop:enable Metrics/AbcSize, Metrics/MethodLength

    private

    # :reek:FeatureEnvy
    def make_api_request(method, params)
      conn.get(method, params) do |req|
        req.headers['User-Agent'] = DefResolver.fetch_user_agent
        req.options.proxy = DefResolver.fetch_proxy_options if DefResolver.proxy.present?
      end
    end

    # :reek:FeatureEnvy
    # :reek:TooManyStatements
    # :reek:DuplicateMethodCall { allow_calls: ['faraday.*', 'DefResolver.logger'] }
    def prepare_connection(faraday)
      faraday.headers['Accept'] = 'application/json'
      faraday.headers['Content-Type'] = 'application/json'
      faraday.request :url_encoded

      faraday.response :json
      faraday.response :logger, DefResolver.logger if DefResolver.logger.present?

      faraday.options[:timeout] = @options.fetch(:timeout, DEFAULT_TIMEOUT)
      faraday.options[:open_timeout] = @options.fetch(:open_timeout, DEFAULT_TIMEOUT)

      faraday.adapter Faraday.default_adapter
    end

    # :reek:UtilityFunction
    def region_leading_zero(region)
      region = region.to_s
      return "0#{region}" if region.size == 1

      region
    end
  end
end
