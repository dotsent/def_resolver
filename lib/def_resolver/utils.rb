module DefResolver
  module Utils
    module_function

    def random_value(value)
      case value
      when Proc
        value.call
      when Array
        value.sample
      else
        value
      end
    end
  end
end
