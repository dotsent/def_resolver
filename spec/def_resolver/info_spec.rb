RSpec.describe DefResolver::Info do
  describe '#new' do
    context 'when pass not exists attribute' do
      let(:attributes) do
        {
          msisdn: '79000000000',
          mcc: '250',
          mnc: '02',
          region_code: '25',
          region_value: 'Краснодарский край',
          extra_not_exist_attr: '11'
        }
      end

      it 'assign only exists attrs' do
        info = described_class.new(attributes)
        %i(msisdn mcc mnc region_code region_value).each do |attr_name|
          expect(info.send(attr_name)).to eq(attributes[attr_name])
        end
      end
    end
  end

  describe '.operator_name' do
    it 'return text operator representation by mnc' do
      expect(described_class.operator_name(described_class::MNC_MEGAFON)).to eq('MEGAFON')
    end

    it 'return nil when unable to convert mnc to text operarator name' do
      expect(described_class.operator_name('UNKNOWN')).to be_nil
    end
  end

  describe '#to_hash' do
    let(:attributes) do
      {
        msisdn: '79000000000',
        mcc: '250',
        mnc: '02',
        region_code: '25',
        region_value: 'Краснодарский край'
      }
    end
    subject { described_class.new(attributes).to_hash }

    it 'return attributes as hash' do
      is_expected.to eq(attributes.stringify_keys)
    end
  end
end
