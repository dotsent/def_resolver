RSpec.describe DefResolver::MegafonMnp do
  def stub_request_for(msisdn)
    stub_request(:get, described_class::DEFAULT_API_BASE_URL + '/mfn/info').with(query: { msisdn: msisdn })
  end

  describe '.can_resolve?' do
    it 'return true if valid russian msisdn passed' do
      expect(described_class.can_resolve?(79001111111)).to be(true)
      expect(described_class.can_resolve?('7(900) 111-11-11')).to be(true)
      expect(described_class.can_resolve?('тел. 7(900) 111-11-11')).to be(true)
    end

    it 'return false if invalid russian msisdn passed' do
      expect(described_class.can_resolve?(380950447788)).to be(false)
      expect(described_class.can_resolve?(7900111111122)).to be(false)
      expect(described_class.can_resolve?(9001111111)).to be(false)
      expect(described_class.can_resolve?('222-22-22')).to be(false)
    end
  end

  describe '#resolve' do
    subject(:resolver) { described_class.new }
    let(:valid_msisdn) { 79023334455 }
    let(:success_response) do
      '{"operator":"МегаФон","operator_id":2,"region":"Ярославская обл.","region_id":76}'
    end

    let(:method_url) { described_class::DEFAULT_API_BASE_URL + '/mfn/info' }

    context 'when set custom proxy' do
      it 'call via proxy'
    end

    context 'when set custom user_agent' do
      let(:expected_user_agent) { 'My Custom UserAgent' }
      before { allow(DefResolver).to receive(:fetch_user_agent).and_return(expected_user_agent) }
      before { stub_request_for(valid_msisdn).to_return(body: success_response) }

      it 'make http request to server with useragent' do
        resolver.resolve(valid_msisdn)

        request = a_request(:get, method_url).with(query: { msisdn: valid_msisdn }).with(
          headers: {
            'Accept' => 'application/json',
            'User-Agent' => expected_user_agent
          }
        )
        expect(request).to have_been_made.once
      end
    end

    context 'when set valid msisdn' do
      context 'when resolve return successful response' do
        before { stub_request_for(valid_msisdn).to_return(body: success_response) }

        it 'make http request to server' do
          resolver.resolve(valid_msisdn)

          request = a_request(:get, method_url).with(query: { msisdn: valid_msisdn }).with(
            headers: {
              'Accept' => 'application/json',
              'User-Agent' => /.*?/
            }
          )
          expect(request).to have_been_made.once
        end

        it 'return DefResolver::Info' do
          result = resolver.resolve(valid_msisdn)

          expect(result.msisdn).to eq(valid_msisdn)
          expect(result.mcc).to eq(DefResolver::Info::MCC_RUSSIA)
          expect(result.mnc).to eq(DefResolver::Info::MNC_MEGAFON)
          expect(result.region_code).to eq(76)
          expect(result.region_value).to eq('Ярославская обл.')
        end
      end

      context 'when resolve return invalid-msisdn response' do
        before { stub_request_for(74959952230).to_return(body: '{"error":"invalid-msisdn"}') }
        it 'raise error' do
          expect { resolver.resolve(74959952230) }
            .to raise_error(DefResolver::Errors::UnableToResolve, 'invalid-msisdn')
        end
      end

      context 'when resolve return invalid html response' do
        before { stub_request_for(74959952230).to_return(body: '<html></html>') }
        it 'raise error' do
          expect { resolver.resolve(74959952230) }.to raise_error(DefResolver::Errors::UnableToResolve)
        end
      end
    end

    context 'when set invalid msisdn' do
      it 'not call remote api and raise exception' do
        invalid_msisdn = 2222222

        expect { resolver.resolve(invalid_msisdn) }.to raise_error(DefResolver::Errors::UnableToResolve)

        expect(a_request(:get, method_url).with(query: { msisdn: invalid_msisdn })).to_not have_been_made
      end
    end
  end
end
