RSpec.describe DefResolver::Utils do
  describe '.random_value' do
    subject { described_class.random_value(value) }

    context 'when user_agent define as array' do
      before { allow(values_list).to receive(:sample).and_return(expected_value) }
      let(:values_list) { %w(val1 val2 val3) }
      let(:expected_value) { values_list.sample }

      let(:value) { values_list }

      it { is_expected.to eq(expected_value) }
    end

    context 'when user_agent define as Proc' do
      let(:expected_value) { 'value from proc' }
      let(:value) do
        proc do
          expected_value
        end
      end

      it { is_expected.to eq(expected_value) }
    end

    context 'when user_agent define as lambda' do
      let(:expected_value) { 'value from lambda' }
      let(:value) { -> { expected_value } }

      it { is_expected.to eq(expected_value) }
    end

    context 'when user_agent define as scalar value' do
      let(:expected_value) { 'custom value' }
      let(:value) { expected_value }

      it { is_expected.to eq(expected_value) }
    end
  end
end
