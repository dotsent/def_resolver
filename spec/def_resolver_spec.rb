RSpec.describe DefResolver do
  it 'has a version number' do
    expect(DefResolver::VERSION).not_to be nil
  end

  describe '.fetch_user_agent' do
    subject { described_class.fetch_user_agent }

    before do
      described_class.setup do |config|
        config.user_agent = user_agent
      end
    end

    context 'when user_agent define as array' do
      before { allow(described_class.user_agent).to receive(:sample).and_return(expected_user_agent) }
      let(:user_agent_list) { %w(ua1 ua2 ua3) }
      let(:expected_user_agent) { user_agent_list.sample }

      let(:user_agent) { user_agent_list }

      it { is_expected.to eq(expected_user_agent) }
    end

    context 'when user_agent define as Proc' do
      let(:expected_user_agent) { 'My custom UA from proc' }
      let(:user_agent) do
        proc do
          expected_user_agent
        end
      end

      it { is_expected.to eq(expected_user_agent) }
    end

    context 'when user_agent define as lambda' do
      let(:expected_user_agent) { 'My custom UA from lambda' }
      let(:user_agent) { -> { expected_user_agent } }

      it { is_expected.to eq(expected_user_agent) }
    end

    context 'when user_agent define as scalar value' do
      let(:expected_user_agent) { 'My custom UA' }
      let(:user_agent) { expected_user_agent }

      it { is_expected.to eq(expected_user_agent) }
    end
  end

  describe '.fetch_proxy_options' do
    subject { described_class.fetch_proxy_options }

    before do
      described_class.setup do |config|
        config.proxy = proxy
      end
    end

    context 'when proxy defin as nil' do
      let(:proxy) { nil }

      it { is_expected.to be_nil }
    end

    context 'when proxy define as array' do
      before { allow(described_class.proxy).to receive(:sample).and_return(expected_proxy) }
      let(:proxy_list) { %w(http://127.0.0.1:8080 http://127.0.0.1:8081) }
      let(:expected_proxy) { proxy_list.sample }

      let(:proxy) { proxy_list }

      it { is_expected.to eq(Faraday::ProxyOptions.from(expected_proxy)) }
    end

    context 'when proxy define as Proc' do
      let(:expected_proxy) { 'http://127.0.0.1:8080' }
      let(:proxy) do
        proc do
          expected_proxy
        end
      end

      it { is_expected.to eq(Faraday::ProxyOptions.from(expected_proxy)) }
    end

    context 'when proxy define as lambda' do
      let(:expected_proxy) { 'http://127.0.0.1:8082' }
      let(:proxy) { -> { expected_proxy } }

      it { is_expected.to eq(Faraday::ProxyOptions.from(expected_proxy)) }
    end

    context 'when proxy define as scalar value' do
      let(:expected_proxy) { 'http://127.0.0.1:8083' }
      let(:proxy) { expected_proxy }

      it { is_expected.to eq(Faraday::ProxyOptions.from(expected_proxy)) }
    end
  end
end
